use std::fmt::{Debug, Display, Formatter};

pub type Result<T> = std::result::Result<T, Error>;

#[derive(Debug)]
pub enum Error {
    UnknownValue,
    BadSyntax,
    UnknownFunction(String),
    TooManyArguments(usize, usize),
    NotEnoughArguments(usize, usize),
}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Error::UnknownValue => f.write_str("Given value is unknown"),
            Error::BadSyntax => f.write_str("Bad syntax"),
            Error::UnknownFunction(name) => write!(f, "The function {} is unknown", name),
            Error::TooManyArguments(given, expected) => write!(f, "Too many arguments, given {}, expected {}", given, expected),
            Error::NotEnoughArguments(given, expected) => write!(f, "Not enough arguments, given {}, expected {}", given, expected),
        }
    }
}

impl std::error::Error for Error {}