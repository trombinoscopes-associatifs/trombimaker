use std::{
    fmt::{Debug, Display, Formatter},
    path::PathBuf,
};

pub type Result<T> = std::result::Result<T, Error>;

#[derive(Debug)]
pub enum Error {
    Toml(toml::de::Error),
    IO(std::io::Error),
    Other(Box<dyn std::error::Error>),

    NoInputFile,

    EmptyString,
    BadClass,
    UnexpectedModifier,
    BadModifier,

    UnknownValue,
    UnknownPerson(String),

    NotAccessible(PathBuf),
}

impl From<toml::de::Error> for Error {
    #[inline]
    fn from(e: toml::de::Error) -> Self {
        Self::Toml(e)
    }
}

impl From<std::io::Error> for Error {
    #[inline]
    fn from(e: std::io::Error) -> Self {
        Self::IO(e)
    }
}

impl From<tera::Error> for Error {
    #[inline]
    fn from(e: tera::Error) -> Self {
        Self::Other(Box::new(e))
    }
}

impl From<toml_edit::TomlError> for Error {
    #[inline]
    fn from(e: toml_edit::TomlError) -> Self {
        Self::Other(Box::new(e))
    }
}

impl From<toml_edit::de::Error> for Error {
    #[inline]
    fn from(e: toml_edit::de::Error) -> Self {
        Self::Other(Box::new(e))
    }
}

/*impl From<directory_deserializer::GenericError<toml::de::Error>> for Error {
    #[inline]
    fn from(value: directory_deserializer::GenericError<toml::de::Error>) -> Self {
        use directory_deserializer::GenericError;
        match value {
            GenericError::IO(e) => Self::IO(e),
            GenericError::Deserializer(e) => Self::Toml(e),
        }
    }
}*/

impl Display for Error {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Error::Toml(e) => Display::fmt(e, f),
            Error::IO(e) => Display::fmt(e, f),
            Error::Other(e) => Display::fmt(e, f),
            Error::BadClass => f.write_str("Bad class for cloth size, possible class are S, M, L"),
            Error::BadModifier => f.write_str("Only X is expected as modifier"),
            Error::UnexpectedModifier => f.write_str("The medium class (M) cannot have X modifier"),
            Error::EmptyString => f.write_str("Given string is empty"),
            Error::UnknownValue => f.write_str("Given value is unknown"),
            Error::UnknownPerson(id) => {
                write!(f, "The given ID `{}` refers to an unknown member", id)
            }
            Error::NotAccessible(path) => write!(
                f,
                "The given path `{}` is not accessible, maybe you don't have the right",
                path.display()
            ),
            Error::NoInputFile => f.write_str("The input file is needed for this operation"),
        }
    }
}

impl std::error::Error for Error {}
