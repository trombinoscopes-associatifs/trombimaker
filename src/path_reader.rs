use crate::error::Result;
use serde::de::DeserializeOwned;
use std::collections::HashSet;
use std::fs::DirEntry;
use std::path::{Path, PathBuf};
use toml_edit::{Document, Item, Table};

fn merge(receiver: &mut Table, emitter: &Table, object_name: Option<&str>) {
    enum Repr<'a> {
        Create(Table),
        Edit(&'a mut Table),
    }

    let receiver_table = match object_name {
        Some(name) => match receiver.get_mut(name) {
            Some(e) => match e.as_table_mut() {
                Some(e) => Repr::Edit(e),
                None => Repr::Create(emitter.clone()),
            },
            None => Repr::Create(emitter.clone()),
        },
        None => Repr::Edit(receiver),
    };

    match receiver_table {
        Repr::Create(table) => {
            receiver.insert(object_name.unwrap(), Item::Table(table));
        }
        Repr::Edit(table) => {
            for (k, v) in emitter {
                if table.contains_key(k) {
                    match v {
                        Item::Table(t) => {
                            merge(table, t, Some(k));
                        }
                        Item::Value(v) => {
                            table.insert(k, Item::Value(v.clone()));
                        }
                        Item::ArrayOfTables(a) => {
                            if let Item::ArrayOfTables(ma) = table.get_mut(k).unwrap() {
                                for t in a {
                                    ma.push(t.clone());
                                }
                            } else {
                                table.insert(k, Item::ArrayOfTables(a.clone()));
                            }
                        }
                        _ => (),
                    };
                } else {
                    table.insert(k, v.clone());
                }
            }
        }
    };
}

fn check_extension(path: &Path, ext: &str) -> bool {
    if let Some(raw_ext) = path.extension() {
        if let Some(to_check) = raw_ext.to_str() {
            to_check == ext
        } else {
            false
        }
    } else {
        false
    }
}

fn entry_task(
    work: &mut Document,
    entry: &DirEntry,
    exclude: &HashSet<PathBuf>,
    root: &Path,
    depth: usize,
) -> Result<()> {
    let raw_path = entry.path();

    let path = match raw_path.strip_prefix(root) {
        Ok(path) => path,
        _ => raw_path.as_path(),
    };

    if exclude.contains(path)
        | path
            .file_name()
            .is_some_and(|v| v.to_str().is_some_and(|v| v.starts_with('.')))
    {
        return Ok(());
    }

    let doc = if let Ok(r#type) = entry.file_type() {
        if r#type.is_file() {
            if check_extension(path, "toml") {
                Some(std::fs::read_to_string(raw_path.as_path())?.parse()?)
            } else {
                None
            }
        } else if r#type.is_dir() {
            read_multiple_files(raw_path.as_path(), exclude, root, depth - 1)?
        } else {
            None
        }
    } else {
        None
    };

    if let Some(document) = doc {
        let name = if let Some(stem) = path.file_stem() {
            stem.to_str().filter(|&stem| stem != "index")
        } else {
            None
        };

        merge(work.as_table_mut(), document.as_table(), name);
    }

    Ok(())
}

fn read_multiple_files(
    from: &Path,
    exclude: &HashSet<PathBuf>,
    root: &Path,
    depth: usize,
) -> Result<Option<Document>> {
    if depth == 0 {
        return Ok(None);
    }

    let metadata = std::fs::metadata(from)?;

    if metadata.is_file() {
        if check_extension(from, "toml") {
            Ok(Some(std::fs::read_to_string(from)?.parse()?))
        } else {
            Ok(None)
        }
    } else {
        let mut work = Document::new();

        for entry in std::fs::read_dir(from)? {
            match entry {
                Ok(entry) => {
                    if let Err(e) = entry_task(&mut work, &entry, exclude, root, depth) {
                        tracing::warn!("`{}` at {}", e, entry.path().display());
                    }
                }
                Err(e) => tracing::warn!("`{}` at {}", e, from.display()),
            }
        }

        Ok(Some(work))
    }
}

pub fn parse_toml_tree<T>(path: &Path, exclude: HashSet<PathBuf>, max_depth: usize) -> Result<T>
where
    T: DeserializeOwned,
{
    let doc = read_multiple_files(path, &exclude, path, max_depth)?.unwrap_or(Document::new());

    Ok(toml_edit::de::from_document(doc)?)
}
