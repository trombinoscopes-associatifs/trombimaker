use color::Color;
use serde::Serialize;
use std::path::PathBuf;

#[derive(Serialize, Clone)]
pub struct Member {
    pub(crate) first_name: String,
    pub(crate) last_name: String,

    pub(crate) nick_name: String,

    pub(crate) picture_name: PathBuf,

    pub(crate) role_name: String,
    pub(crate) role_alias: String,

    pub(crate) email_address: Option<String>,
    pub(crate) phone_number: Option<String>,
}

#[derive(Serialize)]
pub struct Section {
    title: String,

    pub(crate) members: Vec<Member>,
}

#[derive(Serialize)]
pub struct Club {
    pub(crate) name: String,
    pub(crate) id: String,

    pub(crate) logo_name: PathBuf,

    pub(crate) primary_color: Color,
    pub(crate) secondary_color: Color,
    pub(crate) on_primary_text: Color,

    pub(crate) member_number: usize,

    pub(crate) base_members: Vec<Member>,
    pub(crate) sections: Vec<Section>,
}

#[derive(Serialize)]
pub struct Page {
    clubs: Vec<Club>,
}

impl Page {
    #[inline]
    pub(crate) fn with_capacity(cap: usize) -> Self {
        Self {
            clubs: Vec::with_capacity(cap),
        }
    }

    #[inline]
    pub(crate) fn push_back(&mut self, club: Club) {
        self.clubs.push(club);
    }
}

impl Section {
    #[inline]
    pub(crate) fn new(title: &str, member_number: usize) -> Self {
        Self {
            title: title.to_string(),
            members: Vec::with_capacity(member_number),
        }
    }
}
