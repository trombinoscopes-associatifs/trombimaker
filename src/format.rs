use crate::error::{Error, Result};
use crate::{cards, shirt_info};
use color::Color;
use serde::{Deserialize, Deserializer, Serialize, Serializer};
use std::collections::{HashMap, HashSet, LinkedList};
use std::fmt::{Display, Formatter, Write};
use std::path::{Path, PathBuf};

#[cfg(feature = "gen-schema")]
use schemars::JsonSchema;

trait CanEmpty {
    fn is_empty(&self) -> bool;
}

impl CanEmpty for str {
    #[inline]
    fn is_empty(&self) -> bool {
        self.is_empty()
    }
}

fn make_option<T: ToOwned + CanEmpty + ?Sized>(e: &T) -> Option<T::Owned> {
    if e.is_empty() {
        None
    } else {
        Some(e.to_owned())
    }
}

pub fn load<P: AsRef<Path>>(path: P) -> Result<Format> {
    let path = path.as_ref();

    if path.is_dir() {
        Ok(crate::path_reader::parse_toml_tree(
            path,
            HashSet::new(),
            10,
        )?)
    } else if path.is_file() {
        let dat = std::fs::read_to_string(path)?;

        Ok(toml::from_str(dat.as_str())?)
    } else {
        Err(Error::NotAccessible(path.to_path_buf()))
    }
}

fn default_shirt_color() -> String {
    String::from("White")
}

fn default_serigraph_color() -> String {
    String::from("Black")
}

fn default_shirt_quantity() -> usize {
    1
}
#[cfg_attr(feature = "gen-schema", derive(JsonSchema))]
#[derive(Deserialize)]
#[serde(rename_all(deserialize = "kebab-case"))]
pub struct Format {
    pub people: HashMap<String, Person>,
    pub clubs: ListManager<Club>,
    pub roles: HashMap<String, Role>,
}

#[cfg_attr(feature = "gen-schema", derive(JsonSchema))]
#[derive(Deserialize, Default)]
#[serde(rename_all(deserialize = "kebab-case"))]
pub struct ListManager<T> {
    #[serde(flatten)]
    pub list: HashMap<String, T>,

    #[serde(default)]
    pub top_list: Vec<String>,

    #[serde(default)]
    pub order_mode: ClubListOrder,
}

#[cfg_attr(feature = "gen-schema", derive(JsonSchema))]
#[derive(Deserialize, Default)]
#[serde(rename_all(deserialize = "kebab-case"))]
pub enum ClubListOrder {
    #[default]
    AlphabeticalAToZ,
    AlphabeticalZToA,
}

#[cfg_attr(feature = "gen-schema", derive(JsonSchema))]
#[derive(Deserialize)]
#[serde(rename_all(deserialize = "kebab-case"))]
pub struct Person {
    pub picture: PathBuf,
    pub first_name: String,
    pub last_name: String,
    pub email: String,
    pub phone: String,
    pub gender: Gender,
    pub shirt_info: Option<ShirtInfo>,
}

#[cfg_attr(feature = "gen-schema", derive(JsonSchema))]
#[derive(Deserialize)]
#[serde(rename_all(deserialize = "kebab-case"))]
pub struct Club {
    pub name: String,
    pub logo: PathBuf,
    pub primary_color: Color,
    pub secondary_color: Color,
    pub on_primary_color: Color,
    #[serde(default)]
    pub members: Vec<Member>,

    #[serde(default)]
    pub sections: ListManager<Vec<Member>>,

    #[serde(default)]
    pub roles: HashMap<String, Role>,

    #[serde(default = "default_shirt_color")]
    pub shirt_color: String,
    #[serde(default = "default_serigraph_color")]
    pub shirt_serigraph_color: String,
    #[serde(default)]
    pub extra_shirts: Vec<ExtraShirtInfo>,
    #[serde(default)]
    pub give_all_shirts: bool,
}

#[cfg_attr(feature = "gen-schema", derive(JsonSchema))]
#[derive(Deserialize)]
#[serde(rename_all(deserialize = "kebab-case"))]
pub struct ExtraShirtInfo {
    #[serde(default)]
    pub role: String,
    #[serde(default)]
    pub name: String,
    pub size: ShirtSize,
    #[serde(default)]
    pub gender: Gender,
    #[serde(default = "default_shirt_quantity")]
    pub quantity: usize,
    #[serde(default)]
    pub customer_first_name: String,
    #[serde(default)]
    pub customer_last_name: String,
}

#[cfg_attr(feature = "gen-schema", derive(JsonSchema))]
#[derive(Deserialize)]
#[serde(rename_all(deserialize = "kebab-case"))]
pub struct Role {
    // The name of the role
    pub name: String,
    // The female version of the name
    pub variant: Option<String>,

    pub alias: Option<String>,

    #[serde(default)]
    pub official: bool,
}

#[cfg_attr(feature = "gen-schema", derive(JsonSchema))]
#[derive(Deserialize)]
#[serde(rename_all(deserialize = "kebab-case"))]
pub struct Member {
    pub name: String,
    pub role: String,
}

#[cfg_attr(feature = "gen-schema", derive(JsonSchema))]
#[derive(Deserialize)]
#[serde(rename_all(deserialize = "kebab-case"))]
pub struct ShirtInfo {
    pub size: ShirtSize,
    #[serde(default)]
    pub clubs: HashMap<String, ShirtClub>,
}

#[cfg_attr(feature = "gen-schema", derive(JsonSchema))]
#[derive(Deserialize)]
#[serde(rename_all(deserialize = "kebab-case"))]
pub struct ShirtClub {
    pub nickname: String,
    pub role_alias: Option<String>,
    #[serde(default)]
    pub can_purchase: CanPurchase,
}

#[cfg_attr(feature = "gen-schema", derive(JsonSchema))]
#[derive(Default, Copy, Clone, Deserialize, Serialize, Eq, PartialEq)]
pub enum Gender {
    #[serde(rename = "H")]
    #[default]
    Men,
    #[serde(rename = "F")]
    Women,
}

#[derive(Default, Copy, Clone, Eq, PartialEq)]
pub enum CanPurchase {
    Can,
    Cant,
    #[default]
    Undef,
    Given,
}

#[derive(Copy, Clone, Eq, PartialEq)]
pub enum ShirtSize {
    Small(u8),
    Medium,
    Large(u8),
    // Undef,
}

impl Format {
    pub fn generate_shirt_info(&self) -> Result<Vec<shirt_info::ShirtInfo>> {
        use shirt_info::ShirtInfo;

        ShirtInfo::extract_from(self)
    }

    pub fn generate_cards(&self, select: HashSet<&str>) -> cards::Page {
        use cards::*;

        let mut ret = Page::with_capacity(self.clubs.list.len());

        self.populate_clubs(&mut ret, select);

        ret
    }

    pub(crate) fn generate_club_list(
        &self,
        args: &crate::ListClubsArgs,
    ) -> Option<(Vec<Vec<&str>>, usize)> {
        let fields_number = args.count_fields();

        if fields_number > 0 {
            let mut lines = Vec::with_capacity(self.clubs.list.len());
            let mut total_size = 0;

            for (name, info) in &self.clubs.list {
                if !info.members.is_empty() {
                    let mut line = Vec::with_capacity(fields_number);
                    total_size += fields_number;

                    if args.print_id {
                        line.push(name.as_str());
                        total_size += name.len();
                    }

                    if args.print_name {
                        line.push(info.name.as_str());
                        total_size += info.name.len();
                    }

                    lines.push(line);
                }
            }

            if total_size == 0 {
                None
            } else {
                Some((lines, total_size))
            }
        } else {
            eprintln!("No fields were selected, aborting");

            None
        }
    }

    fn populate_clubs(&self, page: &mut cards::Page, select: HashSet<&str>) {
        use cards::*;

        let default_user_cards = self.generate_raw_user_card();

        let list = self.clubs.list_ids(select);

        for club_id in list {
            let club_info = &self.clubs.list[club_id];

            let mut club = Club {
                name: club_info.name.clone(),
                id: club_id.to_string(),
                logo_name: club_info.logo.clone(),
                primary_color: club_info.primary_color.clone(),
                secondary_color: club_info.secondary_color.clone(),
                on_primary_text: club_info.on_primary_color.clone(),
                member_number: 0,
                base_members: Vec::with_capacity(club_info.members.len()),
                sections: Vec::with_capacity(club_info.sections.list.len()),
            };

            let mut member_stack = HashSet::with_capacity(club_info.members.len());

            self.populate_members(
                &mut club.base_members,
                &mut member_stack,
                &mut club.member_number,
                club_info.members.as_slice(),
                &club_info.roles,
                &default_user_cards,
            );

            let sections = club_info.sections.list_ids(HashSet::new());

            for section_name in sections {
                let members = &club_info.sections.list[section_name];

                let mut section = Section::new(section_name, members.len());

                self.populate_members(
                    &mut section.members,
                    &mut member_stack,
                    &mut club.member_number,
                    members.as_slice(),
                    &club_info.roles,
                    &default_user_cards,
                );

                club.sections.push(section);
            }

            page.push_back(club)
        }
    }

    fn populate_members<'a>(
        &self,
        member_list: &mut Vec<cards::Member>,
        member_stack: &mut HashSet<&'a str>,
        member_number: &mut usize,
        members: &'a [Member],
        club_roles: &HashMap<String, Role>,
        user_info: &HashMap<String, (cards::Member, Gender, &Option<ShirtInfo>)>,
    ) {
        for member in members {
            let member_name = member.name.as_str();

            if !member_stack.contains(member_name) {
                member_stack.insert(member_name);
                *member_number += 1;
            }

            if let Some((default_card, gender, shirt_info)) = user_info.get(member.name.as_str()) {
                let mut card = default_card.clone();

                if let Some(role) = club_roles.get(member.role.as_str()) {
                    card.role_name = if let Gender::Men = gender {
                        role.name.as_str()
                    } else {
                        role.variant.as_ref().unwrap_or(&role.name).as_str()
                    }
                    .to_string();

                    card.role_alias = role.alias.as_ref().unwrap_or(&card.role_name).clone();
                } else if let Some(role) = self.roles.get(member.role.as_str()) {
                    card.role_name = if let Gender::Men = gender {
                        role.name.as_str()
                    } else {
                        role.variant.as_ref().unwrap_or(&role.name).as_str()
                    }
                    .to_string();

                    card.role_alias = role.alias.as_ref().unwrap_or(&card.role_name).clone();
                } else {
                    let role = member.role.as_str();

                    card.role_name = role.to_string();
                    card.role_alias = role.to_string();
                };

                if let Some(info) = shirt_info {
                    if let Some(info) = info.clubs.get(member.name.as_str()) {
                        if let Some(alias) = &info.role_alias {
                            card.role_alias = alias.clone();
                        }
                        card.nick_name = info.nickname.clone();
                    }
                }

                member_list.push(card);
            } else {
                eprintln!("Member {} not found", member.name);
            }
        }
    }

    fn generate_raw_user_card(
        &self,
    ) -> HashMap<String, (cards::Member, Gender, &Option<ShirtInfo>)> {
        let mut ret = HashMap::with_capacity(self.people.len());

        for (user_id, user_info) in &self.people {
            ret.insert(
                user_id.clone(),
                (
                    cards::Member {
                        first_name: user_info.first_name.clone(),
                        last_name: user_info.last_name.clone(),
                        nick_name: user_info.first_name.clone(),
                        picture_name: user_info.picture.clone(),
                        role_name: String::new(),
                        role_alias: String::new(),
                        email_address: make_option(user_info.email.as_str()),
                        phone_number: make_option(user_info.phone.as_str()),
                    },
                    user_info.gender,
                    &user_info.shirt_info,
                ),
            );
        }

        ret
    }
}

impl<T> ListManager<T> {
    fn get_club_list<'a, 'b: 'a>(&'b self, reference: HashSet<&'a str>) -> HashSet<&'a str> {
        if !reference.is_empty() {
            reference
        } else {
            let mut ret = HashSet::with_capacity(self.list.len());

            for name in self.list.keys() {
                ret.insert(name.as_str());
            }

            ret
        }
    }

    pub fn list_ids<'a, 'b: 'a>(&'b self, select: HashSet<&'a str>) -> Vec<&'a str> {
        let select = self.get_club_list(select);

        let mut ret = Vec::with_capacity(select.len());

        for id in &self.top_list {
            if select.contains(id.as_str()) {
                ret.push(id.as_str());
            } else if !self.list.contains_key(id.as_str()) {
                eprintln!("The clubs id {id} was not found");
            }
        }

        {
            let mut to_add = Vec::with_capacity(select.len() - ret.len());

            for id in select {
                if !ret.contains(&id) {
                    to_add.push(id);
                }
            }

            to_add.sort_by(|a, b| self.order_mode.cmp(a, b));

            ret.append(&mut to_add);
        }

        ret
    }
}

impl ClubListOrder {
    pub fn cmp(&self, a: &str, b: &str) -> std::cmp::Ordering {
        match self {
            ClubListOrder::AlphabeticalAToZ => a.cmp(b),
            ClubListOrder::AlphabeticalZToA => b.cmp(a),
        }
    }
}

impl ShirtSize {
    fn gen_x(f: &mut Formatter<'_>, num: u8, last: char) -> std::fmt::Result {
        if num >= 3 {
            f.serialize_u8(num)?;
            f.write_char('X')?;
        } else {
            for _ in 0..num {
                f.write_char('X')?;
            }
        }
        f.write_char(last)
    }
}

impl CanPurchase {
    pub fn as_str(&self) -> &'static str {
        match self {
            Self::Cant => "not fund",
            Self::Can => "fund",
            Self::Undef => "unknown",
            Self::Given => "given",
        }
    }
}

impl TryFrom<&str> for ShirtSize {
    type Error = Error;

    fn try_from(value: &str) -> std::result::Result<Self, Error> {
        let mut classes: LinkedList<char> = value.to_ascii_uppercase().chars().collect();

        if let Some(class) = classes.pop_back() {
            match class {
                'S' | 'L' => {
                    let mut counter = 0;
                    while let Some(modifier) = classes.pop_front() {
                        if modifier != 'X' {
                            return Err(Error::BadModifier);
                        }
                        counter += 1;
                    }

                    if class == 'S' {
                        Ok(ShirtSize::Small(counter))
                    } else {
                        Ok(ShirtSize::Large(counter))
                    }
                }
                'M' => {
                    if classes.is_empty() {
                        Ok(ShirtSize::Medium)
                    } else {
                        Err(Error::UnexpectedModifier)
                    }
                }
                _ => Err(Error::BadClass),
            }
        } else {
            Err(Error::EmptyString)
        }
    }
}

impl Display for ShirtSize {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            ShirtSize::Medium => f.write_char('M'),
            ShirtSize::Small(size) => Self::gen_x(f, *size, 'S'),
            ShirtSize::Large(size) => Self::gen_x(f, *size, 'L'),
        }
    }
}

#[cfg(feature = "gen-schema")]
impl JsonSchema for ShirtSize {
    fn schema_name() -> String {
        "ShirtSize".to_string()
    }

    fn json_schema(_gen: &mut schemars::gen::SchemaGenerator) -> schemars::schema::Schema {
        use schemars::schema::{InstanceType, StringValidation};
        use schemars::Map;

        schemars::schema::Schema::Object(schemars::schema::SchemaObject {
            metadata: None,
            instance_type: Some(schemars::schema::SingleOrVec::Single(Box::new(
                InstanceType::String,
            ))),
            format: None,
            enum_values: None,
            const_value: None,
            subschemas: None,
            number: None,
            string: Some(Box::new(StringValidation {
                max_length: None,
                min_length: None,
                pattern: Some("^X?S$|^M$|^X{0,3}L$".to_string()),
            })),
            array: None,
            object: None,
            reference: None,
            extensions: Map::new(),
        })
    }
}

impl From<bool> for CanPurchase {
    fn from(v: bool) -> Self {
        if v {
            Self::Can
        } else {
            Self::Cant
        }
    }
}

impl TryFrom<&str> for CanPurchase {
    type Error = Error;

    fn try_from(value: &str) -> Result<Self> {
        match value {
            "yes" => Ok(Self::Can),
            "no" => Ok(Self::Cant),
            "" => Ok(Self::Undef),
            "can" => Ok(Self::Can),
            "can't" => Ok(Self::Cant),
            "unknown" => Ok(Self::Undef),

            _ => Err(Error::UnknownValue),
        }
    }
}

#[cfg(feature = "gen-schema")]
impl JsonSchema for CanPurchase {
    fn schema_name() -> String {
        "ShirtSize".to_string()
    }

    fn json_schema(_gen: &mut schemars::gen::SchemaGenerator) -> schemars::schema::Schema {
        use schemars::schema::InstanceType;
        use schemars::Map;
        use serde_json::Value;

        schemars::schema::Schema::Object(schemars::schema::SchemaObject {
            metadata: None,
            instance_type: Some(schemars::schema::SingleOrVec::Single(Box::new(
                InstanceType::String,
            ))),
            format: None,
            enum_values: Some(vec![
                Value::String(String::new()),
                Value::String("yes".to_string()),
                Value::String("no".to_string()),
                Value::String("can".to_string()),
                Value::String("can't".to_string()),
                Value::String("unknown".to_string()),
            ]),
            const_value: None,
            subschemas: None,
            number: None,
            string: None,
            array: None,
            object: None,
            reference: None,
            extensions: Map::new(),
        })
    }
}

impl Serialize for ShirtSize {
    fn serialize<S>(&self, serializer: S) -> std::result::Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        serializer.serialize_str(self.to_string().as_str())
    }
}

impl Serialize for CanPurchase {
    fn serialize<S>(&self, serializer: S) -> std::result::Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        serializer.serialize_str(self.as_str())
    }
}

struct ShirtSizeVisitor();

struct CanPurchaseVisitor();

impl<'de> serde::de::Visitor<'de> for ShirtSizeVisitor {
    type Value = ShirtSize;

    fn expecting(&self, formatter: &mut Formatter) -> std::fmt::Result {
        write!(
            formatter,
            "a string with international clothing size as text (XS,S,L,XL,XXL,3XL...)"
        )
    }

    fn visit_str<E>(self, v: &str) -> std::result::Result<Self::Value, E>
    where
        E: serde::de::Error,
    {
        match ShirtSize::try_from(v) {
            Ok(v) => Ok(v),
            Err(_) => Err(serde::de::Error::invalid_value(
                serde::de::Unexpected::Str(v),
                &self,
            )),
        }
    }
}

impl<'de> serde::de::Visitor<'de> for CanPurchaseVisitor {
    type Value = CanPurchase;

    fn expecting(&self, formatter: &mut Formatter) -> std::fmt::Result {
        write!(
            formatter,
            "a string with values can, can't, yes, no or unknown"
        )
    }

    fn visit_bool<E>(self, v: bool) -> std::result::Result<Self::Value, E>
    where
        E: serde::de::Error,
    {
        Ok(if v {
            CanPurchase::Can
        } else {
            CanPurchase::Cant
        })
    }

    fn visit_str<E>(self, v: &str) -> std::result::Result<Self::Value, E>
    where
        E: serde::de::Error,
    {
        match CanPurchase::try_from(v) {
            Ok(v) => Ok(v),
            Err(_) => Err(serde::de::Error::invalid_value(
                serde::de::Unexpected::Str(v),
                &self,
            )),
        }
    }
}

impl<'de> Deserialize<'de> for ShirtSize {
    fn deserialize<D>(deserializer: D) -> std::result::Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        deserializer.deserialize_char(ShirtSizeVisitor())
    }
}

impl<'de> Deserialize<'de> for CanPurchase {
    fn deserialize<D>(deserializer: D) -> std::result::Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        deserializer.deserialize_char(CanPurchaseVisitor())
    }
}
