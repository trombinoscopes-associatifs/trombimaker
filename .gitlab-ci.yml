stages:
  - build
  - upload
  - release
  - coverage

build:
  stage: build
  image: rust:1.79-alpine
  artifacts:
    paths:
      - trombi-maker
      - resources.zip
  variables:
    DEFAULT_TEMPLATE_PATH: template
  script:
    - apk add zip # Required when generating the resources archive
    - apk add musl-dev # Required when compiling some dependencies
    # Generating the resources archive
    - cd resources
    - zip -r ../resources.zip *
    - cd ..
    # Compiling the program
    - cargo build --release
    # Copying the program in the root directory for the artifact sanity
    - cp target/release/trombi-maker trombi-maker
  rules:
    - if: $CI_COMMIT_TAG

upload:
  stage: upload
  image: alpine:latest
  dependencies:
    - build
  script:
    - apk add openssh # Required when uploading data to the binary repo
    # Configuring openssh
    - mkdir ~/.ssh
    - echo $SSH_HOSTKEY >> ~/.ssh/known_hosts
    - cp $SSH_ACCESSKEY ~/.ssh/access
    - chmod 600 ~/.ssh/access
    # Sending data into website
    - ssh -i ~/.ssh/access $SSH_USER@$SSH_HOST "mkdir -p ~/files/$CI_COMMIT_TAG"
    - scp -i ~/.ssh/access trombi-maker $SSH_USER@$SSH_HOST:~/files/$CI_COMMIT_TAG
    - scp -i ~/.ssh/access resources.zip $SSH_USER@$SSH_HOST:~/files/$CI_COMMIT_TAG
  rules:
    - if: $CI_COMMIT_TAG

release:
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - echo "running release_job"
  release:
    tag_name: "$CI_COMMIT_TAG"
    description: "$CI_COMMIT_TAG_MESSAGE"
    assets:
      links:
        - name: "Program"
          url: "https://files.citorva.fr/trombimaker/$CI_COMMIT_TAG/trombi-maker"
          filepath: "/trombi-maker"
        - name: "Default resources and template"
          url: "https://files.citorva.fr/trombimaker/$CI_COMMIT_TAG/resources.zip"
          filepath: "/resources.zip"

coverage:
  stage: coverage
  image: rust:1.67-alpine
  rules:
    - if: $CI_COMMIT_MESSAGE =~ /-draft$/
      when: never
    - if: $CI_PIPELINE_SOURCE == "push"
  script:
    - apk add openssl-dev # Required when compiling tarpaulin
    - apk add musl-dev # Required when compiling some dependencies
    - cargo install cargo-tarpaulin -f
    - cargo tarpaulin
  coverage: '/^\d+.\d+% coverage/'
  artifacts:
    reports:
      coverage_report:
        coverage_format: cobertura
        path: cobertura.xml
